import math

# List Comprehensions

vec = [2, 4, 6]

v = [x * 3 for x in vec]
print(v)

v = [x * 3 for x in vec if x > 3]
print(v)

v = [[x, x * 2] for x in vec]
print(v)

v = [(x, x * 2) for x in vec]
print(v)

vec1 = [2, 4, 6]
vec2 = [4, 3, -9]
v = [x + y for x in vec1 for y in vec2]
print(v)

v = [vec1[i] * vec2[i] for i in range(len(vec1))]
print(v)


def is_prime(x):
    for i in range(2, int(math.sqrt(x)) + 1):
        if x % i == 0:
            return False
    return True


vec = [3, 6, 5, 8, 11]
v = [n for n in vec if is_prime(n)]
print(v)
