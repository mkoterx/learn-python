import math

a = ['What', 'does', 'the', 'fox', 'say']
for i in range(len(a)):
    print(i, a[i])

dic = {'one': 1, 'two': 2, 'three': 3}
print(dic['one'])
print('two' in dic)
print('four' in dic)
print(len(dic))
print(dic.keys())
print(dic.values())
# returns list of touples each containg 
# one key-value pair
print(dic.items())


def is_prime(x):
    for n in range(2, int(math.sqrt(x)) + 1):
        if x % n == 0:
            return False
    return True


print(is_prime(8))


def foo(*args):  # accepts optional numer of arguments
    print(args)


foo(1)
foo(1, 2, 3)
