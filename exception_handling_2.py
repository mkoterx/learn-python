import sys

try:
    file = open('somefile.txt', 'r')
    line = file.readline()
    i = int(line.strip())
except IOError as e:
    print('I/O error({0}): {1}'.format(e.errno, e.strerror))
except ValueError:
    print('Could not convert data to an integer.')
except:
    print('Unexpected error', sys.exc.info()[0])
    raise
