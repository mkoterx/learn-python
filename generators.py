def fib():
    n2 = 1
    n1 = 1
    while True:
        (ret, n1, n2) = (n2, n1 + n2, n1)
        yield ret


fib_gen = fib()
while True:
    b = next(fib_gen)
    print(b)
    if b > 100:
        break
