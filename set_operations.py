"""
A Set is an unordered collection with no duplicate elements. Basic uses include
membership testing and eliminating duplicate entries. Set opjects also support
mathematical operations like union, intersection, difference and symmetric
difference.
"""

basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
distinct_basket = set(basket)
print(distinct_basket)
print('orange' in distinct_basket)  # fast membership testing
print('strawberry' in distinct_basket)

# Set operations on unique letters from two words
a = set('abracadabra')
b = set('alacazam')
print(a)  # unique letters in a
print(a - b)  # letters in a but not in b
print(a | b)  # letters in either a or b
print(a & b)  # letters in both a and b
print(a ^ b)  # letters in a or b but not both
