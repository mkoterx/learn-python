def fib(n):
    a, b = 0, 1
    while b < n:
        print(b, )  # the trailing comma prints in the same line
        a, b = b, a + b


if __name__ == '__main__':
    import sys

    fib(int(sys.argv[1]))
