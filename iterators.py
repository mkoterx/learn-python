from itertools import *


class Fibnum20:
    def __init__(self):
        self.n1 = 1
        self.n2 = 1

    def __next__(self):
        (ret, self.n1, self.n2) = (self.n2, self.n1 + self.n2, self.n1)
        if ret > 20:
            raise StopIteration
        return ret

    def __iter__(self):
        return self


fib_iterator = Fibnum20()
for i in fib_iterator:
    print(i)

print(list(Fibnum20()))
print(sum(Fibnum20()))
print(max(Fibnum20()))
print(min(Fibnum20()))

print(list(islice(Fibnum20(), 6)))
print(list(islice(Fibnum20(), 2, 5)))
# a lot of other useful functions from the itertools module...
