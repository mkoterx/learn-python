table = {'Martin': 32144, 'Alek': 543654, 'Tea': 9876}

for contact in table.items():
    print('{0:10} ==> {1:10}'.format(contact[0], contact[1]))

print('\n')

for (name, phone) in table.items():
    print('{0:10} ==> {1:10}'.format(name, phone))
