from random import randint


class Dice:
    """ Class demostrating a simple playing dice """

    def __init__(self, number_of_sides=6):
        self.number_of_sides = number_of_sides

    def roll(self):
        return randint(1, self.number_of_sides)  # both boundaries are inclusive


dice1 = Dice()
dice2 = Dice(10)

print('Rolling the 6-sided dice 10 times: ')
for i in range(1, 11):
    print(dice1.roll(), ' ', end='')  # default value for end is newline

print()

print('Rolling the 10-sided dice 15 times: ')
for j in range(1, 15):
    print(dice2.roll(), ' ', end='')
