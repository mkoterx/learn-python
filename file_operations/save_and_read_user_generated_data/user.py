import json
import time

class User():

	def __init__(self, username, password, age):
		self.username = username
		self.password = password
		self.age = age
		self.created_on = time.strftime("%d/%m/%Y %H:%M:%S")
	

	def __str__(self):
		return 'username: ' + self.username + ', age: ' + self.age

class Gamer(User):

	def __init__(self, username, password, created_on):
		super().__init__(username, password, created_on)
		self.best_score = 0

	def set_best_score(self, score):
		if score > self.best_score:
			self.best_score = score

	def __str__(self):
		return super().__str__() + ', best score: ' + self.best_score