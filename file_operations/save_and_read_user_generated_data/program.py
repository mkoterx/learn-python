import json
from user import User
from converter import obj_to_dict, dict_to_obj

filename = 'users.json'
users_json = []

def user_exists(filename, username):
	try:
		with open(filename) as file_object:
			contents = file_object.read()
			query = '"username": "' + username + '"'
			return contents.find(query) != -1
	except FileNotFoundError as err:
		print(err)

def write_user_to_file(filename, user):
	try:
		with open(filename) as file_object:
			user_json = json.dumps(user, default=obj_to_dict, sort_keys=True)
			lines = file_object.readlines()
			if (len(lines) == 0):
				lines.append('[\n' + user_json + ',\n]')
			else:
				lines[len(lines) - 1] = user_json + ',\n'
				lines.append(']')
			users_json = lines
	except FileNotFoundError as err:
		print(err)

	with open(filename, 'w') as file_object:
		for line in users_json:
			file_object.write(line)


username = input('Enter username: ')
while (user_exists(filename, username)):
	print('"' + username + '" already exists!') 
	username = input('Enter different username: ')
password = input('Enter password: ')
age = input('Enter age: ')
user = User(username, password, age)
print('User "' + user.username + '" was successfully created')
write_user_to_file(filename, user)

def load_users_from_file(filename):
	try:
		with open(filename) as file_object:
			contents = file_object.readlines()
	except FileNotFoundError as err:
		print(err)
	else:
		users = {}
		contents = contents[1:len(contents) - 1]
		for line in contents:
			line = line[:len(line)-2].strip()
			user = json.loads(line, object_hook=dict_to_obj)
			users[user.username] = user
		return users




