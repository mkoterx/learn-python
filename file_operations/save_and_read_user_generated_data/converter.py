def obj_to_dict(obj):
	"""Convert objects to a dictionary of their representation"""
	d = {
		'__class__': obj.__class__.__name__,
		'__module__': obj.__module__,
	}
	d.update(obj.__dict__)
	return d

def dict_to_obj(d):
	if '__class__' in d:
		class_name = d.pop('__class__')
		module_name = d.pop('__module__')
		module = __import__(module_name)
		class_ = getattr(module, class_name)
		args = {
			key: value for key, value in d.items()	
		}
		inst = class_(**args)
	else:
		inst = d
	return inst
