# 'with' closes the file once access to it is no longer needed

# read the whole file at once
with open('pi_digits.txt') as file_object:
	contents = file_object.read() 
	print(contents)

print()

# read line by line
filename = 'pi_digits.txt'
with open(filename) as file_object:
	for line in file_object:
		print(line.rstrip())

print()

# read and store the lines for further use outside the 'with' block
lines = []
with open(filename) as file_object:
	lines = file_object.readlines()

for line in lines:
	print(line.rstrip())
