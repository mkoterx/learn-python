import sys

if len(sys.argv) == 1:
	print('Usage: python word_counter.py filename')
	sys.exit()

def count_words(filename):
	"""Count the approximate number of words in a file."""
	try:
		with open(filename) as file_object:
			contents = file_object.read()
	except FileNotFoundError:
		msg = 'Sorry, the file [ ' + filename + ' ] does not exist'
		print(msg)
	else:
		words = contents.split()
		num_words = len(words)
		print('The file [ ' + filename + ' ] has about ' + str(num_words) + ' words ')

for filename in sys.argv[1:]:
	count_words(filename)

	
