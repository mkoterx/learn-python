# FUNCTIONAL PROGRAMMING TOOLS IN PYTHON

# filter
import functools


def f(x): return x % 2 != 0 and x % 3 != 0


print(list(filter(f, range(2, 25))))


# map
def cube(x): return x * x * x


print(list(map(cube, range(1, 11))))


# reduce
def add(x, y): return x + y


seq = range(8)
print(list(map(add, seq, seq)))


def equals(x, y):
    return x == y


seq1 = [1, 3, 4, 6]
seq2 = [1, 3, 3, 6]
print(list(map(equals, seq1, seq2)))

print(functools.reduce(add, range(1, 11)))  # just reduce for python 2
