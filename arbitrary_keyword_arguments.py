def build_profile(first, last, **user_info):
    profile = dict()
    profile['first'] = first
    profile['last'] = last
    for (key, value) in user_info.items():
        profile[key] = value
    return profile


user1 = build_profile('albert', 'einstein', location='princeton', field='physics')

for k, v in user1.items():
    print(k, ':', v)
