l = ['one', 'two', 'three']
l.append('four')
l.insert(1, 'one and a half')
print(l)
del l[1]
print(l)
popped = l.pop()  # removes and returns the last element
print(popped)
l.pop(2)  # removes the second element
print(l)

try:
    l.remove('one')
except ValueError as err:
    print('No such element in the list')

print(l)
l.append('ten')
l.append('nine')
l.append('seven')
l.sort()
print(l)
l.sort(reverse=True)
print(l)
print(sorted(l))  # sort temporarily
l.reverse()
print(l)

list_of_numbers = list(range(1, 10, 2))
print(list_of_numbers[0:3])
print(min(list_of_numbers))
print(max(list_of_numbers))
print(sum(list_of_numbers))
ls = list_of_numbers[:]  # makes copy of the list
print(3 in ls)
