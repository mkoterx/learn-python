f = open('data.txt', 'r')  # returns a file object
print(f.read())
f.close()

f = open('data.txt', 'r')
for line in f:
    print(line, )
f.close()

f = open('data.txt', 'r')
print(f.readline())
f.close()

f = open('data.txt', 'r')
print(f.readlines())  # returs list of lines
f.close()

f = open('data.txt', 'w')
f.write('MEAW')  # Writes the content of the argument returning None
f.close()
