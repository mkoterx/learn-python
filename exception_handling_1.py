while True:
    try:
        x = int(input('Please enter a number: '))  # raw_input for python 2
        break  # Program stops after user enters valid number
    except ValueError:
        print('Oops! That was no valid number. Try again...')
